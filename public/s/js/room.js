var socket = io()

setInterval(() => {
  const rand = ('' + Math.random()).substr(2)

  const msg = {
    username : 'chilts',
    room     : 'general',
    msg      : rand,
  }
  console.log('Sending :', msg)

  // tell server to execute 'msg' with the message
  socket.emit('msg', msg)
}, 3000)

socket.on('msg', (data) => {
  console.log('Received :', data)
  var $li = $('<li>')
  $li.append($('<strong>').text(data.username))
  $li.append(' : ')
  $li.append($('<span>').text(data.msg))
  $('#messages').append($li)
})
