// --------------------------------------------------------------------------------------------------------------------
//
// server.js - the server for cssminifier.com
//
// Copyright 2013 AppsAttic Ltd, http://appsattic.com/
//
// --------------------------------------------------------------------------------------------------------------------

"use strict"

// core
const http = require('http')

// npm
const socketIo = require('socket.io')
const LogFmtr = require('logfmtr')
const yid = require('yid')

// local
const log = require('./lib/log.js')
const sessionMiddleware = require('./lib/middleware/session.js')
const app = require('./lib/app.js')

// --------------------------------------------------------------------------------------------------------------------
// setup

process.title = 'cssminifier.com'

// every so often, print memory usage
var memUsageEverySecs = process.env.NODE_ENV === 'production' ? 10 * 60 : 30
setInterval(() => {
  log.withFields(process.memoryUsage()).debug('memory')
}, memUsageEverySecs * 1000)

// --------------------------------------------------------------------------------------------------------------------
// server

const server = http.createServer()
server.on('request', app)

const io = socketIo(server)

io.use((socket, next) => {
  sessionMiddleware(socket.request, socket.request.res, next)
})

io.on('connection', (socket) => {
  console.log('socket-io.connection() - A client Connected')
  console.log('socket-io.session:', socket.request.session)

  socket.on('disconnect', () => {
    console.log('user disconnected')
    log.info('user-disconnected')
  })

  // when the client emits 'new message', this listens and executes
  socket.on('msg', (msg) => {
    msg.id = yid()
    console.log('socket-io.msg - ', msg)
    // we tell all clients (including the sender) to execute 'msg'
    io.emit('msg', msg)
  })
})

const port = process.env.PORT || 9746
server.listen(port, function() {
  log.withFields({ port }).info('server-started')
})

// --------------------------------------------------------------------------------------------------------------------

process.on('SIGTERM', () => {
  log.info('sigterm')
  server.close(() => {
    log.info('exiting')
    process.exit(0)
  })
})

// --------------------------------------------------------------------------------------------------------------------
