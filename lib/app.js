// --------------------------------------------------------------------------------------------------------------------
//
// Copyright (c) 2012.
//
// * Andrew Chilton - https://chilts.org/
// * AppsAttic Ltd  - https://appsattic.com/
// * WebDev.sh      - https://webdev.sh/
//
// --------------------------------------------------------------------------------------------------------------------

// core
const fs = require('fs')

// npm
const express = require('express')
const compress = require('compression')
const favicon = require('serve-favicon')
const bodyParser = require('body-parser')
const moment = require('moment')
const LogFmtr = require('logfmtr')
const cookieSession = require('cookie-session')

// local
const pkg = require('../package.json')
const log = require('./log.js')
const middleware = require('./middleware.js')
const sessionMiddleware = require('./middleware/session.js')

// --------------------------------------------------------------------------------------------------------------------
// setup

const isProd = process.env.NODE_ENV === 'production'
const protocol = 'https'
const nakedDomain = 'cssminifier.com'
const baseUrl = protocol + '://' + nakedDomain

// --------------------------------------------------------------------------------------------------------------------
// application server

const app = express()
app.set('case sensitive routing', true)
app.set('strict routing', true)
app.set('views', __dirname + '/../views')
app.set('view engine', 'pug')
app.enable('trust proxy')

app.locals.pkg = pkg
app.locals.env = process.env.NODE_ENV
app.locals.min = isProd ? '.min' : ''
app.locals.pretty = isProd

// do all static routes first
app.use(compress())
app.use(favicon(__dirname + '/../public/favicon.ico'))

if ( isProd ) {
  const oneMonth = 30 * 24 * 60 * 60 * 1000
  app.use(express.static(__dirname + '/../public/', { maxAge : oneMonth }))
}
else {
  app.use(express.static(__dirname + '/../public/'))
}

app.use(sessionMiddleware)

app.use(bodyParser.urlencoded({
  extended : false,
  limit    : '1mb',
}))

app.use(middleware.log)
app.use(LogFmtr.middleware)

// --------------------------------------------------------------------------------------------------------------------
// Routes

app.get(
  '/',
  middleware.redirectIfLoggedIn,
  (req, res) => {
    res.render('index')
  }
)

app.get(
  '/room/',
  middleware.redirectToSlashIfLoggedOut,
  (req, res) => {
    res.render('room')
  }
)

require('./routes/auth.js')(app)

app.get(
  '/uptime',
  (req, res) => {
    res.setHeader('Content-Type', 'text/plain')
    res.send('' + parseInt(process.uptime(), 10))
  }
)

// --------------------------------------------------------------------------------------------------------------------
// export the app

module.exports = app

// --------------------------------------------------------------------------------------------------------------------
