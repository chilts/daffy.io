// --------------------------------------------------------------------------------------------------------------------

// local
const redis = require('./redis.js')

// --------------------------------------------------------------------------------------------------------------------

function get(key, callback) {
  console.log(`cache.get() - key=${key}`)
  redis.get(key, (err, json) => {
    if (err) return callback(err)
    callback(null, JSON.parse(json))
  })
}

function set(key, val, callback) {
  redis.set(key, JSON.stringify(val), callback)
}

function del(key, callback) {
  redis.del(key, callback)
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = {
  get,
  set,
  del,
}

// --------------------------------------------------------------------------------------------------------------------
