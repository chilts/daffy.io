// --------------------------------------------------------------------------------------------------------------------

// npm
const yid = require('yid')

// local
const log = require('./log.js')
const email = require('./email.js')

// --------------------------------------------------------------------------------------------------------------------

function logit(req, res, next) {
  // add a Request ID
  req._rid = yid()

  // create a RequestID and set it on the `req.log`
  req.log = log.withFields({ rid: req._rid })

  next()
}

function redirectIfLoggedIn(req, res, next) {
  if ( req.session.account ) {
    return res.redirect('/room/')
  }
  next()
}

function redirectToSlashIfLoggedOut(req, res, next) {
  if ( !req.session.account ) {
    res.redirect('/')
    return
  }
  next()
}

// Uses MailGun's API to validate this email address, mainly so that we don't
// have to and we're already using MailGun anyway.
function validateEmail(req, res, next) {
  // ToDo: validate email, but for now presume it is valid.
  email.isValid(req.body.email, (err, isValid) => {
    if (err) return next(err)
    // store the normalised email
    res.locals.email = req.body.email
    next()
  })
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = {
  log : logit,
  redirectIfLoggedIn,
  redirectToSlashIfLoggedOut,
  validateEmail,
}

// --------------------------------------------------------------------------------------------------------------------
